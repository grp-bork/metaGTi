# -*- coding: utf-8 -*-


import os
import sys
import pandas as pd

from subprocess import run
from collections import OrderedDict

from jug import barrier, bvalue, TaskGenerator

try:
    from jug_schedule import ResourcesTaskGenerator
    from jug_schedule.resources import SLURM
except ImportError:
    sys.stderr.write("jug schedule not found or improperly installed\n")
    sys.stderr.write("Reverting to plain jug without resource management\n")

    # NoOp decorator that simply discards passed arguments
    def ResourcesTaskGenerator(*_, **__):
        return TaskGenerator

    SLURM = "SLURM"


class JugVars(dict):
    def __init__(self, name="--imutable_hash--", *args, **kwargs):
        try:
            self._name = name.encode("utf8")
        except AttributeError:
            self._name = name

    def __jug_hash__(self):
        return self._name


def defaultJugVars():
    """Return pre-defined Jug variables usable across runs"""

    VARS = JugVars()

    VARS["tmpdir"] = os.environ.get("TMPDIR", os.path.expanduser("~/tmp"))

    return VARS


def read_sample_file(filepath):
    """Read a mocat sample file and return it as a generator
    """
    with open(filepath) as fh:
        for line in fh:
            if line.startswith("#"):
                continue
            yield line.rstrip("\n")


def count_zeros(nums):
    count = 0
    for i in nums:
        if i != "0":
            count += 1
    return count


VARS = defaultJugVars()
VARS["proj_dir"] = os.path.dirname(__file__)
VARS["metaG_samples"] = os.path.join(VARS["proj_dir"], "metaG.samples")
VARS["metaT_samples"] = os.path.join(VARS["proj_dir"], "metaT.samples")

VARS["db_dir"] = os.path.join(VARS["proj_dir"], "db")
VARS["output_dir"] = os.path.join(VARS["proj_dir"], "output")
VARS["profile_work_dir"] = os.path.join(VARS["proj_dir"], "tmp/{}")
VARS["data_dir"] = os.path.join(VARS["proj_dir"], "data")
VARS["stats_dir"] = os.path.join(VARS["proj_dir"], "stats")

VARS["human"] = os.path.join(
    VARS["db_dir"], "Homo_sapiens.GRCh38.p10.cdna+dna+45S.fna.gz")
VARS["reference"] = os.path.join(
    VARS["db_dir"], "progenomes-v1-rep-v2UL-contigs-min100-human_gut.fna.gz")

VARS["gff"] = os.path.join(
    VARS["db_dir"], "progenomes-v1-emapper_annotations-v2.per-genome-human_gut.gff")

PROFILE_FEATURE = "bactNOG"


resources = {
    "cpu": 8,
    "mem": 64*1024,
    SLURM: {
        "custom": "-t 48:00:00",
    },
}


@ResourcesTaskGenerator(**resources)
def ngless_map_profile(sample, feature, VARS):
    print(">>> Preparing sample", sample, " and mapping against human gut species")
    cmd = [
        "ngless",
        "bin/human_gut-v2UL.ngl",
        "--no-create-report",
        "--trace",
        "-t", VARS['tmpdir'],
        "-j", str(resources["cpu"]),
        sample,
        VARS["data_dir"],
        VARS["metaT_samples"],
        VARS["human"],
        VARS["reference"],
        VARS["gff"],
        feature,
        VARS["output_dir"],
        VARS["stats_dir"],
    ]
    run(cmd, check=True)
    print(">>> Preparing sample", sample, " and mapping against human gut species - DONE")
    return sample


@ResourcesTaskGenerator(**resources)
def ngless_motus(sample, VARS):
    print(">>> Preparing sample", sample, " and profiling with motusv2")
    cmd = [
        "ngless",
        "bin/human_gut-motus-only-v2UL.ngl",
        "--no-create-report",
        "--trace",
        "-t", VARS['tmpdir'],
        "-j", str(resources["cpu"]),
        sample,
        VARS["data_dir"],
        VARS["metaG_samples"],
        VARS["human"],
        VARS["output_dir"],
        VARS["stats_dir"],
    ]
    run(cmd, check=True)
    print(">>> Preparing sample", sample, " and profiling with motusv2 - DONE")
    return sample


profile_resources = {
    "cpu": 4,
    "mem": 64*1024,
    SLURM: {
        "custom": "-t 48:00:00",
    },
}


@ResourcesTaskGenerator(**profile_resources)
def eggnog_profile(sample, feature, mode, VARS):
    print(">>> Profiling sample", sample, " for marker genes")
    infile = os.path.join(VARS["output_dir"], sample + ".human_gut.namesorted.bam")
    current_wd = os.path.realpath(os.getcwd())

    cmd = [
        "ngless",
        "--no-create-report",
        "--trace",
        "-t", VARS["tmpdir"],
        "-j", str(profile_resources["cpu"]),
        os.path.join(current_wd, "bin/profile-eggnog-allmarkers-{}_argv.ngl".format(mode)),
        infile,
        VARS["metaG_samples"],
        sample,
        feature,
        VARS["output_dir"],
    ]
    # Use different workdirs to avoid merging ngless output from different parameters
    workdir = VARS["profile_work_dir"].format(feature)
    os.makedirs(workdir, exist_ok=True)

    run(cmd, check=True, cwd=workdir)
    print(">>> Profiling sample", sample, " for marker genes - DONE")
    return sample


@ResourcesTaskGenerator(cpu=1, mem=1024)
def filter_profile(feature, VARS, min_samples=100):
    def save_line(out, gene, nums):
        out.write("{}\t{}\n".format(gene, '\t'.join(nums)))

    print(">>> Discarding low abundance genes")

    name = "all1"
    template = os.path.join(VARS["output_dir"],
                            "per_marker_allmarkers_eggnog_{}_{}_scaled.tsv")
    outfile = os.path.join(VARS["output_dir"],
                           "filtered_per_marker_allmarkers_eggnog_{}_scaled.tsv")
    outfile = outfile.format(name)

    # The header needs to be saved to the output,
    first_header = True

    with open(outfile, 'w') as out, open(template.format(feature, name)) as fh:
        if first_header:
            out.write(fh.readline())
            first_header = False
        else:
            # Ignore header from all files but the first
            fh.readline()

        for line in fh:
            gene, *nums = line.rstrip('\n').split('\t')

            if gene == "-1":
                gene = "unmapped_{}".format(feature)
                save_line(out, gene, nums)
            else:
                count = count_zeros(nums)

                if count >= min_samples or len(nums) < min_samples:
                    save_line(out, gene, nums)

    print(">>> Discarding low abundance genes - DONE")
    return outfile


@ResourcesTaskGenerator(cpu=1, mem=8*1024)
def count_species(file, min_species=10):
    print(">>> Filter gene matrix for minimum species")

    df = pd.read_csv(file, sep='\t')
    df["gene"], df["species"] = df["Unnamed: 0"].str.split("_", 1).str

    genes = df[["gene", "species"]].groupby(["gene"]).count()
    genes = genes[genes["species"] > min_species]

    # Keep the -1 row even if it was removed
    keep = genes.index.union(["unmapped"])

    d = df[df["gene"].isin(keep)]

    # keep the order of columns and drop the unnamed column
    cols = df.columns.tolist()
    cols = cols[-2:] + cols[1:-2]

    outfile = os.path.splitext(file)[0] + ".{}+species.genes.tsv".format(min_species)
    d[cols].to_csv(outfile, sep='\t', index=False)

    print(">>> Filter gene matrix for minimum species - DONE")
    return outfile


@ResourcesTaskGenerator(cpu=1, mem=64*1024)
def process_profile(inputfile):
    print(">>> Preprocess final gene data")
    cmd = [
        "Rscript",
        "bin/data_wrangle.R",
        inputfile,
    ]
    run(cmd, check=True)
    print(">>> Preprocess final gene data - DONE")
    return inputfile


@ResourcesTaskGenerator(cpu=1, mem=64*1024)
def predict_species_abundance(_dependency):
    print(">>> Predict species abundances")
    cmd = [
        "Rscript",
        "bin/predictSpeciesAbundance.R",
    ]
    run(cmd, check=True)
    print(">>> Predict species abundances - DONE")
    return _dependency


@ResourcesTaskGenerator(cpu=1, mem=64*1024)
def compare_species_abund(_dependency, prediction_source, VARS):
    print(">>> Compare metaG/T species abundance in", prediction_source, "mode")
    rmd_file = "bin/metaGvT_compareSpeciesAbunds.Rmd"
    outfile = os.path.join(VARS["output_dir"],
                           os.path.splitext(os.path.basename(rmd_file))[0])
    if prediction_source == "motus":
        use_selected_species = "FALSE"
        outfile += "_motusAll"
    else:
        use_selected_species = "TRUE"
        outfile += "_mapRF"

    cmd = [
        "R", "-e",
        "rmarkdown::render('{}',output_format='html_document', output_file='{}.html',"
        "params=list(predictionSource='{}', onlyUseSelectedSpecies={}))".format(
            rmd_file, outfile, prediction_source, use_selected_species)
    ]
    run(cmd, check=True)
    print(">>> Compare metaG/T species abundance in", prediction_source, "mode - DONE")
    return _dependency


@TaskGenerator
def parse_stats(sample, dtype, VARS):
    print(">>> Reformat sample statistics")
    template = os.path.join(VARS["stats_dir"], "{}.{}.human_filtered.{}.fxstats")
    sets = ("pair.1", "pair.2", "single")
    result = OrderedDict([("sample", sample)])

    has_read = False
    for s in sets:
        filename = template.format(sample, dtype, s)
        if os.path.isfile(filename):
            has_read = True
            df = pd.read_csv(filename, sep='\t')

            result[s] = int(df.iloc[0]["Max_count"])

    # "sample", "pair.1", "pair.2" and "single"
    if has_read:
        assert result["pair.1"] == result["pair.2"]

        result["paired"] = result["pair.1"] + result["pair.2"]

        result["sequencing"] = "paired"

    # len == 1 is just "sample" because no files match "pair.1", "pair.2" and "single"
    # this happens when the input was single-end from the start and ngless doesn't
    # add either of the suffixes
    elif len(result) == 1:
        template = os.path.join(VARS["stats_dir"], "{}.{}.human_filtered.fxstats")
        filename = template.format(sample, dtype)
        df = pd.read_csv(filename, sep='\t')

        result['single'] = int(df.iloc[0]["Max_count"])
        result['pair.1'] = 0
        result['pair.2'] = 0
        result['paired'] = 0
        result["sequencing"] = "single"

    else:
        raise Exception("Unexpected stats files for {}".format(sample))

    result["total_reads"] = result["paired"] + result["single"]
    result["total_inserts"] = result["pair.1"] + result["single"]
    print(">>> Reformat sample statistics - DONE")
    return result


stats_G = []
stats_T = []

metaT_samples = list(read_sample_file(VARS["metaT_samples"]))
metaG_samples = list(read_sample_file(VARS["metaG_samples"]))

if not len(metaT_samples):
    sys.stderr.write("No metaT samples configured in metaT.samples\n")
    sys.exit(1)

for samplename in metaT_samples:
    sample = ngless_map_profile(samplename, PROFILE_FEATURE, VARS=VARS)
    stats_T.append(parse_stats(sample, "metaT", VARS=VARS))


merged_stats = os.path.join(VARS["output_dir"],
                            "post_human_filter_read_counts.csv")

if len(metaG_samples):
    for samplename in metaG_samples:
        sample = ngless_motus(samplename, VARS=VARS)
        stats_G.append(parse_stats(sample, "metaG", VARS=VARS))

    barrier()

    dfT = pd.DataFrame(bvalue(stats_T)).set_index("sample")
    dfG = pd.DataFrame(bvalue(stats_G)).set_index("sample")

    pd.concat([dfG, dfT], axis=0, sort=False).to_csv(merged_stats)

else:
    sys.stderr.write("No metaG samples configured in metaG.samples\n")
    sys.stderr.write("Skipping metaG / metaT integration and profiling metaT alone\n")

    barrier()

    pd.DataFrame(bvalue(stats_T)).set_index("sample").to_csv(merged_stats)

barrier()

outfile = filter_profile(PROFILE_FEATURE, VARS=VARS)
counts = count_species(outfile)
profile = process_profile(counts)
abund = predict_species_abundance(profile)

if len(metaG_samples):
    dep = compare_species_abund(abund, "mapRfModel", VARS=VARS)
    compare_species_abund(dep, "motus", VARS=VARS)

# vim: ai sts=4 et sw=4
