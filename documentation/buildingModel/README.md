The files in this folder describe the process with which the species abundance predictive model was built.

# Data production

See the files in [bin](bin) for how the gene abundance profiles were made both from the metagenomic data and the metatranscriptomic data. 
Gene profiles generated from metatranscriptomic data were used as the features in the predictive model.

See [human_gut-v2UL.ngl](bin/human_gut-v2UL.ngl) for how the mOTUs abundance profiles were generated from the metagenomic data. 
These species abundances were used as the target of predictions ("truth" variable).

# Data preparation

The model targets (truth values) were prepared using [prepTruthData.R](documentation/buildingModel/01_preparingData/)

The potential predictive features were filtered using 
[preFilterData.R](documentation/buildingModel/01_preparingData/preFilterData.R) and 
[filterSamples.R](documentation/buildingModel/01_preparingData/filterSamples.R). 
The process is described in [dataFilteringRecord.md](documentation/buildingModel/01_preparingData/dataFilteringRecord.md)

After feature filtering, feature abundances were normalised such that the abundance of each feature across all species within a sample sums to 1 (using `normalise.R`)

# Training the model

The model was built using the code in 
[mlr_rf.Rmd](documentation/buildingModel/02_trainingModel/mlr_rf.Rmd) 
which calls a function in [prepDataForModelling.R](documentation/buildingModel/02_trainingModel/prepDataForModelling.R)

The exact run of the code that generated the model being used in this project
is documented in [mlr_rf_2019-06-11_0820.html](documentation/buildingModel/02_trainingModel/mlr_rf_2019-06-11_0820.html).
Model performance is also documented in this file.

