---
title: "Reducing data size prior to model training"
author: "Thea Van Rossum"
date: "2019-11-16"
output: 
  html_document: 
    number_sections: yes
    toc: yes
---

# 1. Initial filter of species and genes

done on server (`theta`) in interactive session, process recorded here.

Function in: `./R/preFilterData.R`

Input data: `formatted_all1.metaT.filtZeroRows.tsv`
Every row is a sample-species pair, and each column is a potential marker gene
This version of the data has rows removed where all gene abundance values are 0 and species abundance is 0

```
allGeneAbunds <- readr::read_tsv("/scratch/rossum/tmp/formatted_all1.metaT.filtZeroRows.tsv",col_types=cols(.default = col_double(),species = col_character(),sample = col_character()))    # this took ~20 min

allGeneAbundsFiltered <- filterData(allGeneAbunds)   # this took ~ 5 min

[1] "Discarding 47% (297339 out of 627509) of sample-species pairs due to pair having < 2 observations."
[1] "Discarding 3% (22 out of 700) of species due to only being observed in < 5 samples."
[1] "Discarded 42303 out of 627509 rows due to low species prevalence."
[1] "Discarding 0% (0 out of 7031) of genes due to gene only being observed in < 50 sample-species pairs.(585206 total sample-species pairs)"
[1] "Discarded 0 out of 7031 genes (columns) due to low species prevalence."

saveRDS(object = allGeneAbundsFiltered, file = "/local/rossum/all_gene_abundances_basicGeneSpeciesFilter.Robj") # this took ~20 min
```

This filtering didn't change the size of the Robj much, but did decrease the number of rows by ~50%

# 2. Filtering samples 

Remove samples that 

- are not from stool (e.g. oral)
- are replicates (keep 1 at random)
- are replicated with a different extraction method (only keep RNAlater)

```
> metaDataPath <- "/g/scb/bork/ralves/projects/ML_MT_markers/data/all.metadata.csv"
> source("/g/scb2/bork/rossum/metaT/metaTMarkerEval/R/filterSamples.R")
> samplesToKeep<-getSamplesToKeep(metaDataPath)
> length(samplesToKeep)
[1] 1289

> length(unique(allGeneAbundsFiltered$sample))
[1] 1440
> dim(allGeneAbundsFiltered)
[1] 585206   7033
> allGeneAbundsFiltered <- allGeneAbundsFiltered[allGeneAbundsFiltered$sample %in% samplesToKeep,]
> length(unique(allGeneAbundsFiltered$sample))
[1] 1285
> dim(allGeneAbundsFiltered)
[1] 517547   7033

```

# 3. More stringent gene/species filter


```
> allGeneAbundsFilteredAgressive <- filterData(allGeneAbundsFiltered,  minObsPerSampleSpeciesPair = 10, minSamplesSpeciesWasSeenIn = 100, minObsPerGene = 10000)
[1] "Discarding 71% (366189 out of 517547) of sample-species pairs due to pair having < 10 observations."
[1] "Discarding 48% (193 out of 401) of species due to only being observed in < 100 samples."
[1] "Discarded 276588 out of 517547 rows due to low species prevalence."
[1] "Discarding 85% (5966 out of 7031) of genes due to gene only being observed in < 10000 sample-species pairs.(240959 total sample-species pairs)"
[1] "Discarded 5966 out of 7031 genes (columns) due to low species prevalence."
```

Now we run again to re-filter rows based on number of non-zero observations after agressive column removal

```
> allGeneAbundsFilteredAgressive <- filterData(allGeneAbundsFilteredAgressive,  minObsPerSampleSpeciesPair = 10, minSamplesSpeciesWasSeenIn = 100, minObsPerGene = 10000)
[1] "Discarding 48% (114963 out of 240959) of sample-species pairs due to pair having < 10 observations."
[1] "Discarding 11% (23 out of 208) of species due to only being observed in < 100 samples."
[1] "Discarded 22270 out of 240959 rows due to low species prevalence."
[1] "Discarding 0% (3 out of 1065) of genes due to gene only being observed in < 10000 sample-species pairs.(218689 total sample-species pairs)"
[1] "Discarded 3 out of 1065 genes (columns) due to low species prevalence."
```

Data remaining has 1062 potential marker genes