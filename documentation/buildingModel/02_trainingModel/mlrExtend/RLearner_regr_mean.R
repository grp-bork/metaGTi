makeRLearner.regr.mean = function() {
  makeRLearnerRegr(
    cl = "regr.mean",
    package = "base",
    par.set = makeParamSet(
      makeLogicalLearnerParam(id = "consider0s", default = TRUE, tunable = TRUE),
      makeNumericLearnerParam(id = "roundResult", default = -1,lower = -1, upper = 3, tunable = TRUE)
    ),
    #par.set = makeParamSet(),
    properties = c("numerics", "factors"),
    name = "mean",
    short.name = "mean",
    note = ""
  )
}

#mlr assumes no special data type for the return value – it will be passed to the predict function
#we are going to define below, so any special code the learner may need can be encapsulated there.
trainLearner.regr.mean = function (.learner, .task, .subset, .weights = NULL, ...) 
{
  #f = getTaskFormula(.task)
  #earth::earth(f, data = getTaskData(.task, .subset), ...)
  #df <- getTaskData(.task, .subset)
  
  # to do: create function that uses the hyperparams
  return(NULL)
}

predictLearner.regr.mean = function (.learner, .model, .newdata, ...) 
{
  #predict(.model$learner.model, newdata = .newdata)[, 1L]
  predictions <- apply(.newdata,1,mean)
  # test
  #predictions <- sample(x = predictions,size = length(predictions),replace = F)
  return(predictions)
}

