#!/usr/bin/env sh

set -eu

echo ">>> Fetching required databases <<<"
wget "http://swifter.embl.de/~ralves/metaGT_tool_data/progenomes-v1-emapper_annotations-v2.per-genome-human_gut.gff"
wget "http://swifter.embl.de/~ralves/metaGT_tool_data/progenomes-v1-rep-v2UL-contigs-min100-human_gut.fna.gz"
wget "http://swifter.embl.de/~ralves/metaGT_tool_data/Homo_sapiens.GRCh38.p10.cdna+dna+45S.fna.gz"
wget "http://swifter.embl.de/~ralves/metaGT_tool_data/mlr-rf_2019-06-11-0820_HMP2-IBDMDB_model1.rds"

echo ">>> Verifying downloads <<<"
md5sum -c db.checksums

echo ">>> All databases downloaded <<<"
